

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                      Alumni
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>alumni">Alumni</a></li>
                        <li class="active">Edit Alumni</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Edit Alumni</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="update" method="post" class="form-horizontal" action="<?=base_admin();?>modul/alumni/alumni_action.php?act=up">
                      <div class="form-group">
                        <label for="nim" class="control-label col-lg-2">nim</label>
                        <div class="col-lg-10">
                          <input type="text" data-rule-number="true" name="nim" value="<?=$data_edit->nim;?>" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="nama" class="control-label col-lg-2">nama</label>
                        <div class="col-lg-10">
                          <input type="text" name="nama" value="<?=$data_edit->nama;?>" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="angkatan" class="control-label col-lg-2">angkatan</label>
                        <div class="col-lg-10">
                          <select name="angkatan" data-placeholder="Pilih angkatan..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("tahun") as $isi) {

                  if ($data_edit->angkatan==$isi->tahun) {
                    echo "<option value='$isi->id' selected>$isi->tahun</option>";
                  } else {
                  echo "<option value='$isi->id'>$isi->tahun</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="email" class="control-label col-lg-2">email</label>
                        <div class="col-lg-10">
                          <input type="text"  data-rule-email="true" name="email" value="<?=$data_edit->email;?>" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="alamat" class="control-label col-lg-2">alamat</label>
                        <div class="col-lg-10">
                          <textarea id="editbox" name="alamat" class="editbox"><?=$data_edit->alamat;?> </textarea>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="telepon" class="control-label col-lg-2">telepon</label>
                        <div class="col-lg-10">
                          <input type="text" data-rule-number="true" name="telepon" value="<?=$data_edit->telepon;?>" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="nama_kantor" class="control-label col-lg-2">nama_kantor</label>
                        <div class="col-lg-10">
                          <input type="text" name="nama_kantor" value="<?=$data_edit->nama_kantor;?>" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="alamat_kantor" class="control-label col-lg-2">alamat_kantor</label>
                        <div class="col-lg-10">
                          <textarea id="editbox" name="alamat_kantor" class="editbox"><?=$data_edit->alamat_kantor;?> </textarea>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="nomor_kantor" class="control-label col-lg-2">nomor_kantor</label>
                        <div class="col-lg-10">
                          <input type="text" data-rule-number="true" name="nomor_kantor" value="<?=$data_edit->nomor_kantor;?>" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->

                      <input type="hidden" name="id" value="<?=$data_edit->id;?>">
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
                    <a href="<?=base_index();?>alumni" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
 