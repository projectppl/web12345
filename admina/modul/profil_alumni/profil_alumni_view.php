
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Profil Alumni
                    </h1>
                       <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>profil-alumni">Profil Alumni</a></li>
                        <li class="active">Profil Alumni List</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                  <h3 class="box-title">List Profil Alumni</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="dtb_manual" class="table table-bordered table-striped">
                                   <thead>
                                     <tr>
                           <th style="width:25px" align="center">No</th>
                          <th>NIM</th>
													<th>Nama</th>
													<th>Angkatan</th>
													<th>Email</th>
													<th>Alamat</th>
													<th>Telepon</th>
													<th>Nama Kantor</th>
													<th>Alamat Kantor</th>
													<th>Nomor Kantor</th>
													<th>Foto</th>
													
                          <th>Action</th>
                         
                        </tr>
                                      </thead>
                                        <tbody>
                                         <?php 
      $dtb=$db->fetch_custom("select alumni.nim,alumni.nama,alumni.angkatan,alumni.email,alumni.alamat,alumni.telepon,alumni.nama_kantor,alumni.alamat_kantor,alumni.nomor_kantor,alumni.foto,alumni.id from alumni ");
      $i=1;
      foreach ($dtb as $isi) {
        ?><tr id="line_<?=$isi->id;?>">
        <td align="center"><?=$i;?></td><td><?=$isi->nim;?></td>
<td><a href="<?= base_index()?>profil-alumni/add_profil/<?=$isi->id;?>"><?=$isi->nama;?></a></td>
<td><?=$isi->angkatan;?></td>
<td><?=$isi->email;?></td>
<td><?=$isi->alamat;?></td>
<td><?=$isi->telepon;?></td>
<td><?=$isi->nama_kantor;?></td>
<td><?=$isi->alamat_kantor;?></td>
<td><?=$isi->nomor_kantor;?></td>
<td><?=$isi->foto;?></td>

        <td>
        <a href="<?=base_index();?>profil-alumni/detail/<?=$isi->id;?>" class="btn btn-success btn-flat"><i class="fa fa-eye"></i></a> 
        <?=($role_act["up_act"]=="Y")?'<a href="'.base_index().'profil-alumni/edit/'.$isi->id.'" class="btn btn-primary btn-flat"><i class="fa fa-pencil"></i></a>':"";?>  
        <?=($role_act["del_act"]=="Y")?'<button class="btn btn-danger hapus btn-flat" data-uri="'.base_admin().'modul/profil_alumni/profil_alumni_action.php" data-id="'.$isi->id.'"><i class="fa fa-trash-o"></i></button>':"";?>
        </td>
        </tr>
        <?php
        $i++;
      }
      ?>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
        <?php
       foreach ($db->fetch_all("sys_menu") as $isi) {
                      if ($path_url==$isi->url) {
                          if ($role_act["insert_act"]=="Y") {
                    ?>
          <a href="<?=base_index();?>profil-alumni/tambah" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah</a>
                          <?php
                          } 
                       } 
}
?>  
                </section><!-- /.content -->
        
