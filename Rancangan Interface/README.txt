Alur aplikasi Via Interface

Interface yang sudah dibuat:
1. Beranda Web Alumni
   Berisi menu awal yang telah direncanakan dan juga berisi tentang berita yang ada di fakultas
   dan juga universitas. jika berita di klik, maka akan muncul ke halaman berita untuk mendapatkan
   informasi berita yang diinginkan dengan lebih jelas dan lengkap.
2. Registrasi Alumni
   Berisi tentang registrasi data alumni yang dimasukan oleh alumni untuk mendapatkan username dan
   password. ALumni akan menginputkan data alumni secara lengkap dari mulai Nama, Angkatan dan juga NIM
   dan Tempat Kerja+Alamat Kerja nya untuk kemudian di verifikasi oleh admin. Nantinya jika
   data telah terverifikasi, username dan password akan dikirimkan ke email alumni.
3. Layanan Karir
   Berisi tentang layanan karir dan beasiswa yang tersedia. Data lowongan kerja dan beasiswa
   yang ada dapat diisi oleh admin ataupun alumni sendiri yang telah terverifikasi datanya dan
   diinput di halaman alumni. Halaman ini bertujuan untuk membantu memberikan informasi terkait
   lowongan kerja atau bursa kerja yang tersedia dan peluang beasiswa yang tersedia.
4. Tentang Kami
   Berisi profil dari Ikatan Alumni Teknik Informatika UIN SGD Bandung beserta susunan organisasi
   yang ada di Ikatan Alumni.


Interface Admin:
1. Beranda Admin
   Berisi tampilan tombol untuk menuju ke menu selanjutnya sesuai dengan keperluan yang dimiliki
   oleh admin.
2. Edit Data Admin
3. Data Alumni
4. Verifikasi Data
5. Berita
6. Lowongan Kerja / Beasiswa
7. Agenda / Acara


Interface Alumni:
1. Beranda Alumni
   Berisi tampilan data alumni yang sebelumnya di input saat memasukan data untuk di verifikasi,
   alumni dapat mengubah data tersebut untuk mendapatkan data secara up-to-date.
2. Edit Data Alumni
3. Cari Alumni
4. Info Lowongan Kerja
5. Info Beasiswa
6. Info Agenda / Acara