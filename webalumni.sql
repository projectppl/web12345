-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02 Des 2016 pada 14.47
-- Versi Server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `webalumni`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `agenda`
--

CREATE TABLE IF NOT EXISTS `agenda` (
`id` int(11) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `keterangan` varchar(1000) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `alumni`
--

CREATE TABLE IF NOT EXISTS `alumni` (
`id` int(11) NOT NULL,
  `nim` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `angkatan` year(4) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `alamat` varchar(1000) DEFAULT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  `nama_kantor` varchar(100) DEFAULT NULL,
  `alamat_kantor` varchar(1000) DEFAULT NULL,
  `nomor_kantor` varchar(50) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `alumni`
--

INSERT INTO `alumni` (`id`, `nim`, `nama`, `angkatan`, `email`, `alamat`, `telepon`, `nama_kantor`, `alamat_kantor`, `nomor_kantor`, `foto`, `username`, `password`) VALUES
(2, '1137050177', 'Rahadian Irvan MT', 2015, '', '', '', '', '', '', 'iphone-5-home-screen-ios-7.jpg', 'colep', '50e45c8df823d6d1543c26b8f4a827f0'),
(3, '1137050190', 'Raisa M', 2010, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'raisa', '4b8ed057e4f0960d8413e37060d4c175');

-- --------------------------------------------------------

--
-- Struktur dari tabel `beasiswa`
--

CREATE TABLE IF NOT EXISTS `beasiswa` (
`id` int(11) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `keterangan` varchar(1000) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
`id` int(11) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `isi_berita` varchar(1000) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lowongan_kerja`
--

CREATE TABLE IF NOT EXISTS `lowongan_kerja` (
`id` int(11) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `keterangan` varchar(1000) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_group_users`
--

CREATE TABLE IF NOT EXISTS `sys_group_users` (
`id` int(11) NOT NULL,
  `level` varchar(50) DEFAULT NULL,
  `deskripsi` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `sys_group_users`
--

INSERT INTO `sys_group_users` (`id`, `level`, `deskripsi`) VALUES
(1, 'admin', 'Administrator'),
(2, 'alumni', 'alumni');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_menu`
--

CREATE TABLE IF NOT EXISTS `sys_menu` (
`id` int(11) NOT NULL,
  `nav_act` varchar(150) DEFAULT NULL,
  `page_name` varchar(150) DEFAULT NULL,
  `url` varchar(100) NOT NULL,
  `main_table` varchar(150) DEFAULT NULL,
  `icon` varchar(150) DEFAULT NULL,
  `urutan_menu` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `dt_table` enum('Y','N') NOT NULL,
  `tampil` enum('Y','N') NOT NULL,
  `type_menu` enum('main','page') NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data untuk tabel `sys_menu`
--

INSERT INTO `sys_menu` (`id`, `nav_act`, `page_name`, `url`, `main_table`, `icon`, `urutan_menu`, `parent`, `dt_table`, `tampil`, `type_menu`) VALUES
(14, NULL, 'master data', '', NULL, 'fa-book', 1, 0, 'Y', 'Y', 'main'),
(15, 'data_alumni', 'data alumni', 'data-alumni', 'alumni', 'fa-circle', 1, 14, 'Y', 'Y', 'page'),
(17, 'profil_alumni', 'profil alumni', 'profil-alumni', 'alumni', 'fa-tag', 2, 0, 'Y', 'Y', 'page'),
(18, NULL, 'konten', '', NULL, 'fa-cloud', 1, 0, 'Y', 'Y', 'main'),
(19, 'berita', 'berita', 'berita', 'berita', 'fa-bullhorn', 1, 18, 'Y', 'Y', 'page'),
(20, 'beasiswa', 'beasiswa', 'beasiswa', 'beasiswa', 'fa-desktop', 1, 18, 'Y', 'Y', 'page'),
(21, 'lowongan_kerja', 'lowongan kerja', 'lowongan-kerja', 'lowongan_kerja', 'fa-certificate', 1, 18, 'Y', 'Y', 'page'),
(22, 'agenda', 'agenda', 'agenda', 'agenda', 'fa-calendar', 2, 0, 'Y', 'Y', 'page');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_menu_role`
--

CREATE TABLE IF NOT EXISTS `sys_menu_role` (
`id` int(11) NOT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `read_act` enum('Y','N') DEFAULT NULL,
  `insert_act` enum('Y','N') DEFAULT NULL,
  `update_act` enum('Y','N') DEFAULT NULL,
  `delete_act` enum('Y','N') DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data untuk tabel `sys_menu_role`
--

INSERT INTO `sys_menu_role` (`id`, `id_menu`, `group_id`, `read_act`, `insert_act`, `update_act`, `delete_act`) VALUES
(21, 14, 1, 'Y', 'Y', 'Y', 'Y'),
(22, 14, 2, 'Y', 'N', 'N', 'N'),
(23, 15, 1, 'Y', 'Y', 'Y', 'Y'),
(24, 15, 2, 'Y', 'N', 'N', 'N'),
(27, 17, 1, 'Y', 'Y', 'Y', 'Y'),
(28, 17, 2, 'Y', 'Y', 'Y', 'Y'),
(29, 18, 1, 'Y', 'Y', 'Y', 'Y'),
(30, 18, 2, 'N', 'N', 'N', 'N'),
(31, 19, 1, 'Y', 'Y', 'Y', 'Y'),
(32, 19, 2, 'N', 'N', 'N', 'N'),
(33, 20, 1, 'Y', 'Y', 'Y', 'Y'),
(34, 20, 2, 'N', 'N', 'N', 'N'),
(35, 21, 1, 'Y', 'Y', 'Y', 'Y'),
(36, 21, 2, 'N', 'N', 'N', 'N'),
(37, 22, 1, 'Y', 'Y', 'Y', 'Y'),
(38, 22, 2, 'N', 'N', 'N', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_users`
--

CREATE TABLE IF NOT EXISTS `sys_users` (
`id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL DEFAULT '0',
  `last_name` varchar(50) NOT NULL DEFAULT '0',
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `foto_user` varchar(150) DEFAULT NULL,
  `id_group` int(11) DEFAULT NULL,
  `aktif` enum('Y','N') NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `sys_users`
--

INSERT INTO `sys_users` (`id`, `first_name`, `last_name`, `username`, `password`, `email`, `date_created`, `foto_user`, `id_group`, `aktif`) VALUES
(1, 'Admin', 'UIN BANDUNG', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'alumni@uinsgd.ac.id', '2015-01-26', '10965740_10206190197982755_22114424_n.jpg', 1, 'Y'),
(2, 'Alumni', 'alumni', 'alumni', '9855f5cdff0306ae33a49f89e087ccbc', 'alumni@uinsgd.ac.id', '2016-11-12', 'logo.png', 2, 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahun`
--

CREATE TABLE IF NOT EXISTS `tahun` (
`id` int(11) NOT NULL,
  `tahun` year(4) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data untuk tabel `tahun`
--

INSERT INTO `tahun` (`id`, `tahun`) VALUES
(1, 2000),
(2, 2001),
(3, 2002),
(4, 2003),
(5, 2004),
(6, 2005),
(7, 2006),
(8, 2007),
(9, 2008),
(10, 2009),
(11, 2010),
(12, 2011),
(13, 2012),
(14, 2013),
(15, 2014),
(16, 2015),
(17, 2016);

-- --------------------------------------------------------

--
-- Struktur dari tabel `verifikasi_alumni`
--

CREATE TABLE IF NOT EXISTS `verifikasi_alumni` (
`id` int(11) NOT NULL,
  `nim` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `angkatan` year(4) DEFAULT NULL,
  `verifikasi` enum('Y','N') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alumni`
--
ALTER TABLE `alumni`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `beasiswa`
--
ALTER TABLE `beasiswa`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_berita_sys_users` (`id_admin`);

--
-- Indexes for table `lowongan_kerja`
--
ALTER TABLE `lowongan_kerja`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_group_users`
--
ALTER TABLE `sys_group_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_menu`
--
ALTER TABLE `sys_menu`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_menu_role`
--
ALTER TABLE `sys_menu_role`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_sys_menu_role_sys_menu` (`id_menu`), ADD KEY `FK_sys_menu_role_sys_users` (`group_id`);

--
-- Indexes for table `sys_users`
--
ALTER TABLE `sys_users`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_sys_users_sys_group_users` (`id_group`);

--
-- Indexes for table `tahun`
--
ALTER TABLE `tahun`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verifikasi_alumni`
--
ALTER TABLE `verifikasi_alumni`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `alumni`
--
ALTER TABLE `alumni`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `beasiswa`
--
ALTER TABLE `beasiswa`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lowongan_kerja`
--
ALTER TABLE `lowongan_kerja`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_group_users`
--
ALTER TABLE `sys_group_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sys_menu`
--
ALTER TABLE `sys_menu`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `sys_menu_role`
--
ALTER TABLE `sys_menu_role`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `sys_users`
--
ALTER TABLE `sys_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tahun`
--
ALTER TABLE `tahun`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `verifikasi_alumni`
--
ALTER TABLE `verifikasi_alumni`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `berita`
--
ALTER TABLE `berita`
ADD CONSTRAINT `FK_berita_sys_users` FOREIGN KEY (`id_admin`) REFERENCES `sys_users` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `sys_menu_role`
--
ALTER TABLE `sys_menu_role`
ADD CONSTRAINT `FK_sys_menu_role_sys_group_users` FOREIGN KEY (`group_id`) REFERENCES `sys_group_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_sys_menu_role_sys_menu` FOREIGN KEY (`id_menu`) REFERENCES `sys_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `sys_users`
--
ALTER TABLE `sys_users`
ADD CONSTRAINT `FK_sys_users_sys_group_users` FOREIGN KEY (`id_group`) REFERENCES `sys_group_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
